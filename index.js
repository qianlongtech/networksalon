/*global $*/
$(document).ready(function () {
    'use strict';
    var init, bind;
    bind = function () {
        $('.btn-submit').click(function () {
            var uname = $('input[name=uname]').val(),
                tel = $('input[name=tel]').val(),
                uunit = $('input[name=uunit]').val(),
                uduties = $('input[name=uduties]').val();
            $.post('http://cgi.qianlong.com/diaocha/2016yqh/save.asp', 'uname=' + uname + '&tel=' + tel + '&uunit=' + uunit + '&uduties=' + uduties);
            window.alert('报名成功，感谢参与');
        });
    };
    init = (function () {
        $('.main').fadeIn();
        $('.main').fullpage({
            navigation: true
        });
        bind();
    }());
});
